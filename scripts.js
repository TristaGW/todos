$(document).ready ( function () {
    $('.listItemInput').focus();

// Add to to do
    var todo = $('#todo');
    todo.on( 'keypress',function(event) {
        var itemToAdd = todo.val().trim();
            keycode = (event.keycode || event.which);
        if (keycode === 13) { //on enter
            $('.listItems').append('<form class="input"><input type="checkbox" name="item" class="item" value="'+itemToAdd +' " /> <span class="value">' + itemToAdd + '</span></form>');
            $('.listItems .value').inlineEdit(); //Edits!
            todo[0].value = "";
        }
    });

// Delete All from list
   $('#checkAll').change( function(e) {
        console.log('checked');
        if ( $(this).is(':checked') ) {
                $('.item').each( function(item) {
                  this.checked = 'checked';
                  remove_item(e, this);
              });
        }else{
            $('.item').each( function(item) {
                  this.checked = null;
                  remove_item(e, this);
            });
        }
    });

    function remove_item(e, elem) {
      console.log(item);
        var item = $(e.el),
            self = elem;
        if ( $(self).is(':checked')) {
            console.log('is checked');
            var parentElem = $(self).parent();
            parentElem.wrap ("<strike>"), function() {
                $(self).parent().remove();
            }
        } else {
            console.log('is unchecked');
            var parentElem = $(self).parent();
            parentElem.unwrap();
        };
    };
    
   // Remove from todo 
    $('.listItems').on('change', '.item', function(e) { 
        remove_item(e,this) 
    }); 
 

    //Sortable List
    $('.listItems').sortable();

// Yay!!
    $('#container').on('submit', function() {
        return false;
    });

});